# Build

FROM node:alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

# Deploy

FROM node:alpine

WORKDIR /app

COPY package*.json ./

RUN npm install --production

COPY --from=0 /app/dist ./dist
COPY --from=0 /app/serve.json ./serve.json

EXPOSE 3000

ENTRYPOINT [ "npx", "serve", "--no-clipboard" ]
