<!doctype html>

<html lang="en">
  <head>
    <title>Jaiden Douglas</title>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta name="author" content="Jaiden Douglas" />
    <meta name="description" content="My portfolio." />

    <link rel="icon" href="/favicon.png" />

    <script type="module" src="/index.js" defer></script>
  </head>

  <body>
    <nav
      class="container has-pt-8 has-pb-8 is-flex has-items-center has-justify-between"
    >
      <a href="/" class="is-not-underlined has-w-64">
        <b class="has-size-3">ja1den</b>
      </a>

      <div class="is-inline-flex">
        <a class="navlink" href="https://gitlab.com/ja1den" aria-label="GitLab">
          <i class="is-block" icon-name="gitlab"></i>
        </a>
        <a class="navlink" href="https://github.com/ja1den" aria-label="GitHub">
          <i class="is-block" icon-name="github"></i>
        </a>
      </div>
    </nav>

    <section id="introduction" class="container has-pt-16 has-pb-16">
      <header class="has-pb-8">
        <h1>Pointbase</h1>
        <a href="https://github.com/ja1den/pointbase">Repository</a>
      </header>

      <p class="has-w-192 has-w-full-mobile">
        Pointbase is a scoring system for school sports days, designed for the
        junior campus at my school. It's built with
        <a href="https://expressjs.com/">Express</a>,
        <a href="https://mysql.com/">MySQL</a>,
        <a href="https://pugjs.org/">Pug</a>, and
        <a href="https://socket.io/">Socket.IO</a>.
      </p>
      <p class="has-w-192 has-w-full-mobile">
        The system provides teachers with access to a form where they can
        allocate points as students complete events. The form has several
        different layouts, allowing each sport to have a different scoring
        system. The system also provides a live results dashboard, which updates
        as new points are allocated.
      </p>
      <p class="has-pb-8 has-w-192 has-w-full-mobile">
        The application implements a role-based permissions system, where
        regular users can only access the results dashboard, teachers can access
        the results form, and admin users can access pages for managing events,
        houses, and sports. This ensures that results cannot be modified by any
        unauthorised parties.
      </p>

      <img
        src="./media/01.png"
        width="2560"
        height="1440"
        alt="screenshot of the Pointbase dashboard page"
        class="is-rounded has-w-192 has-w-full-mobile"
      />
    </section>

    <section id="design" class="container has-pt-16 has-pb-16">
      <h2 class="has-pb-8">Design</h2>

      <p class="has-w-192 has-w-full-mobile">
        To begin, I communicated with the sports department at the junior campus
        to understand their existing, paper-based scoring system. Following
        these conversations, I began planning my digital replacement. This
        involved creating an ER Diagram detailing the database structure, design
        drawings outlining the layout and content of each page, and flowcharts
        solidifying how different parts of the system would work.
      </p>
      <p class="has-pb-8 has-w-192 has-w-full-mobile">
        The most difficult part of the design was working out how to support
        events with different scoring systems, whilst still allowing for new
        events to be created in the future. In the end, I devised a set of four
        generic scoring schemes which covered all the existing events, and
        provided an interface for creating new events based on these schemes.
      </p>

      <div class="columns has-m-0 has-w-192 has-w-full-mobile">
        <div
          class="column has-p-0 has-mr-0-mobile has-mr-4-tablet has-mt-4 has-mb-4 is-full-mobile"
        >
          <div
            class="is-flex has-justify-center has-items-center has-h-full has-p-4 has-bg-muted is-rounded"
          >
            <img
              src="./media/02.png"
              width="1630"
              height="878"
              alt="ER Diagram illustrating the Pointbase database"
            />
          </div>
        </div>
        <div
          class="column has-p-0 has-ml-0-mobile has-ml-4-tablet has-mt-4 has-mb-4 is-full-mobile"
        >
          <div
            class="is-flex has-justify-center has-items-center has-h-full has-p-4 has-bg-muted is-rounded"
          >
            <img
              src="./media/03.png"
              width="2402"
              height="2122"
              alt="flowchart illustrating the logic for Pointbase CRUD API routes"
            />
          </div>
        </div>
      </div>
    </section>

    <section id="implementation" class="container has-pt-16 has-pb-16">
      <h2 class="has-pb-8">Implementation</h2>

      <p class="has-w-192 has-w-full-mobile">
        The application was built using
        <a href="https://nodejs.org/">Node.js</a> with
        <a href="https://expressjs.com/">Express</a>, using the
        <a href="https://pugjs.org/">Pug</a> templating engine. This stack lent
        itself to the model-view-controller (MVC) design pattern, which made the
        resulting codebase easy to navigate and reason about. Further, the
        ability to create templates allowed me to break the user interface into
        reusable components, such as the top navigation bar, making it easier to
        maintain.
      </p>
      <p class="has-w-192 has-w-full-mobile">
        The events, houses, sports, and results were stored in a
        <a href="https://mysql.com/">MySQL</a> database, and I used the
        <a href="https://sequelize.org/">Sequelize</a> ORM to automate table
        creation and simplify the process of writing database queries.
      </p>
      <p class="has-pb-8 has-w-192 has-w-full-mobile">
        Finally, I used <a href="https://socket.io/">Socket.IO</a> to implement
        the live-update functionality of the dashboard page. This was made easy
        by its built-in support for
        <a href="https://expressjs.com/">Express</a>, which I was already using
        for page rendering and API routes.
      </p>

      <div class="columns has-m-0 has-w-192 has-w-full-mobile">
        <div
          class="column has-p-0 has-mr-0-mobile has-mr-4-tablet has-mt-4 has-mb-4 is-full-mobile"
        >
          <img
            src="./media/04.png"
            width="2560"
            height="1440"
            alt="screenshot of the Pointbase results form page"
            class="is-rounded"
          />
        </div>
        <div
          class="column has-p-0 has-ml-0-mobile has-ml-4-tablet has-mt-4 has-mb-4 is-full-mobile"
        >
          <img
            src="./media/05.png"
            width="2560"
            height="1440"
            alt="screenshot of the Pointbase results CRUD table page"
            class="is-rounded"
          />
        </div>
      </div>
    </section>

    <footer
      class="container has-pt-16 has-pb-8 is-flex has-items-center has-justify-between"
    >
      <p class="is-inline">
        Built by <b>ja1den</b
        ><span class="is-hidden-mobile">, with &#x2764; and <b>HTML</b></span
        >.
      </p>

      <div class="is-inline-flex">
        <a class="navlink" href="https://gitlab.com/ja1den" aria-label="GitLab">
          <i class="is-block" icon-name="gitlab"></i>
        </a>
        <a class="navlink" href="https://github.com/ja1den" aria-label="GitHub">
          <i class="is-block" icon-name="github"></i>
        </a>
      </div>
    </footer>
  </body>
</html>
