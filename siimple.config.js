import base from "@siimple/preset-base";
import dark from "@siimple/preset-dark";

export default {
  ...base,
  colorModes: {
    dark: {
      ...dark.colors,
    },
  },
  useColorModes: true,
  useColorModesMediaQuery: true,
  useCssVariables: true,
};
